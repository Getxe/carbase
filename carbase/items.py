# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FirmItem(scrapy.Item):

	TYPE 				= scrapy.Field()
	IE_XML_ID 			= scrapy.Field()	# уникальный ID
	IE_NAME				= scrapy.Field()	# Название
	IE_ACTIVE			= scrapy.Field()	# Активность(Y/N) (всегда Y)
	IE_DETAIL_PICTURE	= scrapy.Field()	# Относительный url картинки
	IE_DETAIL_TEXT		= scrapy.Field()	# Описание
	IE_DETAIL_TEXT_TYPE	= scrapy.Field()	# Тип описания(html/text) (всегда text)
	IE_CODE				= scrapy.Field()	# Символьный код (из IE_NAME)


class PartItem(scrapy.Item):

	TYPE 				= scrapy.Field()
	IE_XML_ID			= scrapy.Field()	# уникальный ID
	IE_NAME				= scrapy.Field()	# Название
	IE_ACTIVE			= scrapy.Field()	# Активность(Y/N) (всегда Y)
	IE_DETAIL_PICTURE	= scrapy.Field()	# Относительный url картинки
	IE_DETAIL_TEXT		= scrapy.Field()	# Описание
	IE_DETAIL_TEXT_TYPE	= scrapy.Field()	# Тип описания(html/text) (всегда text)	
	IE_CODE				= scrapy.Field()	# Символьный код (из IE_NAME)
   #IP_PROP12			= scrapy.Field()	# IDшники модификацийи из таблицы модификаций
	IP_PROP10			= scrapy.Field()	# Код детали
	IP_PROP14			= scrapy.Field()	# Кроссы
	IP_PROP11			= scrapy.Field()	# ориг/неориг (всегда ориг)
	FIRM_NAME			= scrapy.Field()	# название фирмы производителя
   #IC_GROUP0			= scrapy.Field()	# Категория
   #IC_CODE0			= scrapy.Field()	# Код категории

class ModItem(scrapy.Item):


	TYPE 				= scrapy.Field()
	URL 				= scrapy.Field()
	PARSED 				= scrapy.Field()
	IE_XML_ID			= scrapy.Field()	# уникальный ID
	IE_NAME				= scrapy.Field()	# Название
	IE_ACTIVE			= scrapy.Field()	# Активность(Y/N) (всегда Y)
	IE_DETAIL_PICTURE	= scrapy.Field()	# Относительный url картинки
	IE_DETAIL_TEXT 	 	= scrapy.Field()	# Описание
	IE_DETAIL_TEXT_TYPE = scrapy.Field()	# Тип описания(html/text) (всегда text)	
	IE_CODE				= scrapy.Field()	# Символьный код (из IE_NAME)
	IP_PROP4			= scrapy.Field()	# код двигателя
	IP_PROP1			= scrapy.Field()	# Начало выпуска
	IP_PROP2			= scrapy.Field()	# Конец выпуска
	IP_PROP3			= scrapy.Field()	# Кузов
	IP_PROP15			= scrapy.Field()	# Объем
	IP_PROP6			= scrapy.Field()	# мощность KW
	IP_PROP7			= scrapy.Field()	# мощность л.с.
	IP_PROP5			= scrapy.Field()	# Топливо
	IC_GROUP0			= scrapy.Field()	# Марка машины
	IC_CODE0			= scrapy.Field()	# Код марки
	IC_GROUP1			= scrapy.Field()	# Модель машины
	IC_CODE1			= scrapy.Field()	# Код модели


class RelatedModAndPart(scrapy.Item):
	IE_XML_ID				= scrapy.Field()	# уникальный ID
	TYPE 					= scrapy.Field()
	CONNECTION_TYPE 		= scrapy.Field()
	PART_FIRM 				= scrapy.Field()
	PART_CODE 				= scrapy.Field()
	MODEL_NAME 				= scrapy.Field()
	MODEL_FIRM 				= scrapy.Field()
	MODEL_MODEL 			= scrapy.Field()

class RelatedParts(scrapy.Item):
	TYPE = scrapy.Field()
	MAIN = scrapy.Field()
	CROSS = scrapy.Field()