# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
from slugify import slugify

def connection_type(str):
	if str == u'\u041d\u0430\u0440\u043e\u0434\u043d\u044b\u0439 \u043f\u043e\u0434\u0431\u043e\u0440':
		return 'PARTS_NARPO'
	elif str == u'\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u0438 \u0434\u043b\u044f \u0422\u041e':
		return 'PARTS_ZAPTO'
	elif str == u'\u0410\u043a\u0441\u0435\u0441\u0441\u0443\u0430\u0440\u044b':
		return 'PARTS_ACCESSORIES'
	elif str == u'\u0411\u0435\u0437 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438':
		return 'PARTS_WITHOUTCAT'

class CarbasePipeline(object):
	def process_item(self, item, spider):
		return item

class AbstractMongoPipeline(object):

	def __init__(self, mongo_uri, mongo_db):
		self.mongo_uri = mongo_uri
		self.mongo_db = mongo_db

	def run_with(self, spider):
		return self.pipeline_name in getattr(spider, 'pipelines', [])

	def get_collection(self):
		return self.db[self.collection_name]

	@classmethod
	def from_crawler(cls, crawler):
		return cls(
			mongo_uri=crawler.settings.get('MONGO_URI'),
			mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
		)

	def open_spider(self, spider):
		self.client = pymongo.MongoClient(self.mongo_uri)
		self.db = self.client[self.mongo_db]

	def close_spider(self, spider):
		self.client.close()

class MainMongoPipeLine(AbstractMongoPipeline):
	
	pipeline_name = 'main_pipe'
	collection_name = 'test_1'

	def process_item(self, item, spider):
		if not self.run_with(spider):
			return item
		if item.get('IE_NAME', None) == None and item['TYPE'] in ['model', 'part', 'firm']:
			return item

		ID = self.get_collection().count() + 1

		if item['TYPE'] == 'model':
			obj = self.get_collection().find_one(
				{ 
					'TYPE'		: 'model',
					'IC_CODE0'	: item['IC_CODE0'],
					'IC_CODE1'	: item['IC_CODE1'],
					'IE_CODE'	: item['IE_CODE']
				})
			if obj == None:
				new_obj = dict(item)
				new_obj['IE_XML_ID'] = ID
				new_obj['IE_DETAIL_TEXT']      	= ''
				new_obj['IE_DETAIL_TEXT_TYPE'] 	= 'text'
				new_obj['IE_ACTIVE']           	= 'Y'
				self.get_collection().insert_one(new_obj)
			#else:
				#IE_XML_ID = obj['IE_XML_ID']
				#new_obj = dict(item)
				#new_obj['IE_XML_ID'] = IE_XML_ID
				#self.get_collection().replace_one(obj, new_obj)

		elif item['TYPE'] == 'firm':
			if item['IE_NAME'] == u'None':
				return item
			obj = self.get_collection().find_one(
				{ 
					'TYPE'		: 'firm',
					'IE_NAME': item['IE_NAME']
				})
			item['IE_CODE']=slugify(item['IE_NAME'], word_boundary=True, save_order=True, separator="_")
			new_obj = dict(item)
			if obj == None:
				new_obj['IE_XML_ID'] = ID
				self.get_collection().insert_one(new_obj)
			#else:
				#IE_XML_ID = obj['IE_XML_ID']
				#new_obj['IE_XML_ID'] = IE_XML_ID
				#self.get_collection().replace_one(obj, new_obj)

		elif item['TYPE'] == 'part':
			if item['FIRM_NAME'] == u'None' or item['IP_PROP10'] == u'None':
				return item

			firmOBJ = self.get_collection().find_one({'TYPE': 'firm', 'IE_NAME': item['FIRM_NAME']})
			obj = self.get_collection().find_one(
				{ 
					'TYPE'		: 'part',
					'IP_PROP10'	: item['IP_PROP10'],
					'IP_PROP13'	: firmOBJ['IE_XML_ID']
				})
			new_obj = dict(item)
			new_obj['IP_PROP13'] = firmOBJ['IE_XML_ID']
			new_obj['IE_CODE']=  slugify(item['IE_NAME'], word_boundary=True, save_order=True, separator="_")+"_"+slugify(item['FIRM_NAME'], word_boundary=True, save_order=True, separator="_")+"_"+slugify(item['IP_PROP10'], word_boundary=True, save_order=True, separator="_")
			if obj == None:
				new_obj['IP_PROP10'] = str(item['IP_PROP10'])
				new_obj['IE_XML_ID'] = ID
				new_obj['IP_PROP14'] = []
				self.get_collection().insert_one(new_obj)
			#else:
				#IE_XML_ID = obj['IE_XML_ID']
				#new_obj = dict(item)
				#new_obj['IE_XML_ID'] = IE_XML_ID
				#self.get_collection().replace_one(obj, new_obj)

		elif item['TYPE'] == 'connection':
			model_filter = {'TYPE': 'model', 'IC_GROUP1': item['MODEL_MODEL'], 'IC_GROUP0': item['MODEL_FIRM'], 'IE_NAME': item['MODEL_NAME']}
			
			model = self.get_collection().find_one(model_filter)
			part = self.get_collection().find_one({'TYPE':'part', 'FIRM_NAME': item['PART_FIRM'], 'IP_PROP10': item['PART_CODE']})
			
			CONNECTION_TYPE = connection_type(item['CONNECTION_TYPE'])
			values = model.get(CONNECTION_TYPE, [])
			values.append(part['IE_XML_ID'])
			self.get_collection().find_one_and_update(model_filter, { '$set' : { CONNECTION_TYPE : values}})

			#model = self.get_collection().find_one()
			#new_connection = { 'TYPE':'connection', 'IE_XML_ID': ID, 'CONNECTION_TYPE': item['CONNECTION_TYPE'], 'MODEL_ID': model['IE_XML_ID'], 'PART_ID': part['IE_XML_ID'] }
			#self.get_collection().insert_one(new_connection)

		elif item['TYPE'] == 'samepart':

			for main_part in item.get('MAIN', []):
				main_filter = {'TYPE':'part', 'IE_NAME': main_part['IE_NAME'], 'IP_PROP10': main_part['IP_PROP10'], 'FIRM_NAME': main_part['FIRM_NAME']}
				main = self.get_collection().find_one(main_filter)
				if main == None:
					return item
				for cross_part in item.get('CROSS', []):
					cross = self.get_collection().find_one({'TYPE':'part', 'IE_NAME': cross_part['IE_NAME'], 'IP_PROP10': cross_part['IP_PROP10'], 'FIRM_NAME': cross_part['FIRM_NAME']})
					if cross == None:
						return item
					related = main['IP_PROP14']
					related.append(cross['IE_XML_ID'])
					self.get_collection().find_one_and_update(main_filter, { '$set': { 'IP_PROP14' : related } })
					
		return item

 
 

