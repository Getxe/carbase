# -*- coding: utf-8 -*-
import re, scrapy, time
from selenium import webdriver
from slugify import slugify
from scrapy.loader import ItemLoader
from scrapy.selector import Selector
from carbase.items import FirmItem, ModItem, PartItem, RelatedModAndPart, RelatedParts
import pymongo

def my_proxy(PROXY_HOST,PROXY_PORT):
        fp = webdriver.FirefoxProfile()
        # Direct = 0, Manual = 1, PAC = 2, AUTODETECT = 4, SYSTEM = 5
        print PROXY_PORT
        print PROXY_HOST
        fp.set_preference("network.proxy.type", 1)
        fp.set_preference("network.proxy.http",PROXY_HOST)
        fp.set_preference("network.proxy.http_port",int(PROXY_PORT))
        fp.set_preference("general.useragent.override","whater_useragent")
        fp.update_preferences()
        return webdriver.Firefox(firefox_profile=fp)


def clean_str(str):
    return re.sub("^\s+|\n|\r|\s+$", '', str)

def delete_spaces(str):
    return re.sub("\n|\r|\s+", '', str)

def divide_date(str):
    return clean_str(str).split('-')

class ModelSpider(scrapy.Spider):
    name = "model_spider"
    pipelines = ['main_pipe']
    start_urls = [
        "http://parts.rolf24.ru/?page=carbase",
    ]

    marka_row = 3
    marka_col = 1

    #min = 2
    model_row = 3

    allowed_domains = ["z24.ru", "admin.abcp.ru",]

    def parse(self, response):
        for href in response.css("tr:nth-child("+str(self.marka_row)+") > td:nth-child("+str(self.marka_col)+") > a::attr('href')"): 
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_models_dir)

    def parse_models_dir(self, response):
        for href in response.css("tr:nth-child("+str(self.model_row)+") > td > a::attr('href')"):# :nth-child(2) :nth-child(1)
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_modif_dir)

    def parse_modif_dir(self, response):
        for href in response.css("tr > td > a::attr('href')"):# :nth-child(2) :nth-child(1)
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.main_parse)

    def main_parse(self, response):
        self.logger.info('==============================\nparsing MODIFICATION PAGE\n=================================== %s', response.url)
        # ПАРСИНГ МОДЕЛИ АВТОМОБИЛЯ 
        IE_NAME             = response.css("div.breadcrumbs > strong::text").extract_first()
        IC_GROUP0           = response.css("div.breadcrumbs > a:nth-child(3)::text").extract_first()
        IC_GROUP1           = response.css("div.breadcrumbs > a:nth-child(5)::text").extract_first()
        model = ModItem()
        model['TYPE']               =   'model'
        model['IE_NAME']            =   IE_NAME
        model['IE_DETAIL_PICTURE']  =   response.css("div.carPhoto > img::attr('src')").extract_first()
        model['IP_PROP1']           =   divide_date(response.xpath("//table[@class='modificationDescription']//tr[1]/td/text()").extract_first().encode('utf-8'))[0]
        model['IP_PROP2']           =   divide_date(response.xpath("//table[@class='modificationDescription']//tr[1]/td/text()").extract_first().encode('utf-8'))[1]
        model['IP_PROP3']           =   response.xpath("//table[@class='modificationDescription']//tr[2]/td/text()").extract_first()
        model['IP_PROP4']           =   clean_str(str(response.xpath("//table[@class='modificationDescription']//tr[3]/td/text()").extract_first()))
        model['IP_PROP5']           =   response.xpath("//table[@class='modificationDescription']//tr[7]/td/text()").extract_first()
        model['IP_PROP6']           =   response.xpath("//table[@class='modificationDescription']//tr[5]/td/text()").extract_first()
        model['IP_PROP7']           =   response.xpath("//table[@class='modificationDescription']//tr[6]/td/text()").extract_first()
        model['IP_PROP15']          =   response.xpath("//table[@class='modificationDescription']//tr[4]/td/text()").extract_first()
        model['IC_GROUP0']          =   IC_GROUP0
        model['IC_GROUP1']          =   IC_GROUP1
        model['IC_CODE0']           =   slugify(IC_GROUP0, word_boundary=True, save_order=True, separator="_")
        model['IC_CODE1']           =   slugify(IC_GROUP1, word_boundary=True, save_order=True, separator="_")
        model['IE_CODE']            =   slugify(IE_NAME, word_boundary=True, save_order=True, separator="_")
        model['URL'] = response.url
        model['PARSED'] = False

        yield model

class PartSpider(scrapy.Spider):
    name = "part_spider"
    pipelines = ['main_pipe']

    def __init__(self):
        scrapy.Spider.__init__(self)
        self.br = webdriver.Firefox()

    def start_requests(self):
        client = pymongo.MongoClient()
        self.c = client['carbase']['test_1']
        for model in self.c.find({'TYPE':'model', 'PARSED' : False}):
            request = scrapy.Request(model['URL'], self.parse)
            model['PARSED']=True
            request.meta['model'] = model
            yield request

    def parse(self, response):
        
        item = response.meta['model']
        model_filter = {'TYPE': 'model', 'IC_GROUP1': item['IC_GROUP1'], 'IC_GROUP0': item['IC_GROUP0'], 'IE_NAME': item['IE_NAME']}
        self.c.find_one_and_update(model_filter, { '$set' : { 'PARSED' : True}})

        MODEL_NAME      = response.css("div.breadcrumbs > strong::text").extract_first()
        MODEL_FIRM      = response.css("div.breadcrumbs > a:nth-child(3)::text").extract_first()
        MODEL_MODEL     = response.css("div.breadcrumbs > a:nth-child(5)::text").extract_first()
        for tr in response.css("div.mcArticlesArea>form>table>tr"):
            url = response.urljoin(tr.css("td.prices> a::attr('href')").extract_first())
            
            connect = RelatedModAndPart(
                TYPE            = 'connection',
                CONNECTION_TYPE = tr.css("td:nth-child(3)::text").extract_first(default=u'None'),
                MODEL_NAME      = MODEL_NAME,
                MODEL_FIRM      = MODEL_FIRM,
                MODEL_MODEL     = MODEL_MODEL
                )
            request = scrapy.Request(url, callback=self.load_data)
            request.meta['connect'] = connect
            yield request

        #переход по пагинации
        next_page = response.css("div.mcArticlesArea > form > div > ul > li:last-child > a::attr('href')").extract_first()
        if next_page!=None:
            url = response.urljoin(next_page)
            request = scrapy.Request(url, callback=self.parts_next_page)
            request.meta['model'] = response.meta['model']
            yield request


    def parts_next_page(self, response):
        self.logger.info('==============================\nparsing NEXT PAGE\n=================================== %s', response.url)

        MODEL_NAME      = response.css("div.breadcrumbs > strong::text").extract_first()
        MODEL_FIRM      = response.css("div.breadcrumbs > a:nth-child(3)::text").extract_first()
        MODEL_MODEL     = response.css("div.breadcrumbs > a:nth-child(5)::text").extract_first()
        for tr in response.css("div.mcArticlesArea>form>table>tr"):
            url = response.urljoin(tr.css("td.prices> a::attr('href')").extract_first())
            
            connect = RelatedModAndPart(
                TYPE            = 'connection',
                CONNECTION_TYPE = tr.css("td:nth-child(3)::text").extract_first(default=u'None'),
                MODEL_NAME      = MODEL_NAME,
                MODEL_FIRM      = MODEL_FIRM,
                MODEL_MODEL     = MODEL_MODEL
                )
            request = scrapy.Request(url, callback=self.load_data)
            request.meta['connect'] = connect
            yield request
        #переход по пагинации
        next_page = response.css("div.mcArticlesArea > form > div > ul > li:last-child > a::attr('href')").extract_first()
        if next_page!=None:
            url = response.urljoin(next_page)
            request = scrapy.Request(url, callback=self.parts_next_page)
            request.meta['model'] = response.meta['model']
            yield request


    def load_data(self, response):
        self.logger.info('==============================parsing PARTS PAGE PAGE=================================== %s', response.url)

        if response.xpath("//*[@id='showMoreAnalogs']").extract_first()!= None:
            self.br = my_proxy("211.141.82.247","8118")
            self.logger.info('load_more found!')
            self.br.get(response.url)
            time.sleep(5)
            loadAll = self.br.find_element_by_id("showMoreAnalogs")
            loadAll.click()
            time.sleep(5)
            selector =  Selector(text=self.br.page_source)
        else:
            selector =  Selector(response=response)

        main = []
        crosses = []

        for link in selector.css("#searchResultsTable>tbody>tr"):

            firm_name = clean_str(str(link.css("td.resultBrand>div>a::text").extract_first(default=u'None').encode('utf-8')))
            part_name = clean_str(str(link.css("td.resultDescription::text").extract_first(default=u'None').encode('utf-8')))

            code_selector = link.css("td:nth-child(3)")
            cross = 0
            if code_selector.css("div.brand::text").extract_first() != None:
                part_code = code_selector.css("div.brand::text").extract_first().encode('utf-8')
                cross = -1
            else:
                part_code = code_selector.css("::text").extract_first(default=u'None').encode('utf-8')
                cross = 1

            part_code = delete_spaces(part_code)  

            firm=FirmItem(TYPE='firm',IE_NAME=firm_name)
            yield firm

            part = PartItem(TYPE='part')
            part['IE_NAME']    = part_name
            part['IP_PROP10']  = part_code
            part['FIRM_NAME']  = firm_name
            yield part

            if part['FIRM_NAME'] != 'None' and part['IP_PROP10']!= 'None' and part['IE_NAME']!='None':
                if cross > 0:
                    main.append(part)
                elif cross < 0:
                    crosses.append(part)

            href = link.css("a.brandInfoLink::attr('href')").extract_first()
            if href!=None:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_firm)
                request.meta['firm'] = firm
                yield request

        yield RelatedParts(
            TYPE='samepart',
            MAIN=main,
            CROSS=crosses
            )
        
        if len(main)>0:
            connect = response.meta['connect']
            connect['PART_FIRM'] = main[0]['FIRM_NAME']
            connect['PART_CODE'] = main[0]['IP_PROP10']
            yield connect

    def parse_firm(self, response):
        firm = response.meta['firm']

        err = re.findall(r"<h3>.+h3>", response.body)
        if len(err)>0:
            yield firm
        else:
            desc = re.findall(r'class=\'brandAnnotation\'>.+div><br', response.body)
            img = re.findall(r"src=\'.+alt", response.body)
            if len(img)>0:
                firm['IE_DETAIL_PICTURE']=img[0][5:-5] 
            if len(desc)>0:
                firm['IE_DETAIL_TEXT']=desc[0][24:-10]
            yield firm
            """url = re.findall(r'_blank\'.+a>', response.body)
            if len(url) > 0:
                url = url[0][8:-5]
                request = scrapy.Request(url, callback=self.parse_firm_extra)
                request.meta['firm'] = firm
                yield request
            else:
                yield firm"""

    """def parse_firm_extra(self, response):
        firm = response.meta['firm']
        name = response.css("div.parent-block > h2")
        img = response.css("div.parent-block > img::attr('href')")
        desc = response.css("div.parent-block > div")

        firm['IE_DETAIL_PICTURE']=img
        firm['IE_DETAIL_TEXT']=desc
        yield firm"""