# Установка: #
1. установить MongoDB ([https://www.mongodb.org/downloads](https://www.mongodb.org/downloads))
2. установить зависимости: 

```
#!console

pip install -r requirements.txt
easy_install pymongo
```

# Использование: #
3. запустить сервер MongoDB из папки проекта: (db.conf - конфиг в папке проекта) 
```
#!console

path\to\mongodb\bin\mongod -f db.conf 
```

4. запустить парсер: 
```
#!console

scrapy crawl model_spider
scrapy crawl part_spider
```

5. подождать
6. выгрузить в CSV из MongoDB:

```
#!console

F:\MongoDB\bin\mongoexport -d carbase -c test_1 -q "{ 'TYPE' : 'part'}" --fieldFile part.txt --type csv --out "part.csv"
F:\MongoDB\bin\mongoexport -d carbase -c test_1 -q "{ 'TYPE' : 'model'}" --fieldFile model.txt --type csv --out "model.csv"
F:\MongoDB\bin\mongoexport -d carbase -c test_1 -q "{ 'TYPE' : 'firm'}" --fieldFile firm.txt --type csv --out "firm.csv"
```